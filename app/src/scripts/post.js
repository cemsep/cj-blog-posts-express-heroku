export default class Post {
    constructor(post) {
        this.userId = post.userId;
        this.id = post.id;
        this.title = post.title;
        this.body = post.body;
    }
    /**
    get title() {
        return this.title;
    }

    get body(){
        return this.body;
    }

    get id() {
        return this.id;
    }
     **/
}