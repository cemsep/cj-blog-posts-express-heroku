'use strict'

/**
 * Dependencies
 * @ignore
 */
const cwd = process.cwd()
const path = require('path')
const express = require('express')
const morgan = require('morgan')

/**
 * Dotenv
 * @ignore
 */
require('dotenv').config()

/**
 * Express
 * @ignore
 */
const { PORT: port = 3000 } = process.env
const app = express()

app.use(morgan('tiny'))
app.use(express.static(path.join(cwd, 'public')))

// GET /message
app.get('/message', (req, res) => {
  res.json({ 
      message: 'Hello, World!'
    })
})

/**
 * Launch app
 * @ignore
 */
app.listen(port, () => console.log(`Example app listening on port ${port}!`))




